module.exports = {
  VERSION1: '/api/v1',
  FUNCTIONS: '/exec',
  FILE: '/file',
  USER: '/user',
  GET_ERROR: '/errors'
}