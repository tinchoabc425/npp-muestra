const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const chalk = require('chalk');
const cors = require('cors');
const app = express();
const pack = require('../package');
const pgp = require('pg-promise');
const path = require('path');
// if NODE_ENV value not define then dev value will be assign 
mode = process.env.NODE_ENV || 'dev';
// mode can be access anywhere in the project
const config = require('config').get(mode);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

// Image handler - https://github.com/richardgirges/express-fileupload
app.use(fileUpload());

// use only when you want to see the metric related to express app
// app.use(require('express-status-monitor')());

require('./routes')(app);
const dir = path.join(__dirname, 'assets');
app.use('/upload', express.static(dir));


const start = () => (
  app.listen(config.port, () => {
    console.log(chalk.yellow('.......................................'));
    console.log(chalk.green(config.name));
    console.log(chalk.green(`Port:\t\t${config.port}`));
    console.log(chalk.green(`Mode:\t\t${config.mode}`));
    console.log(chalk.green(`App version:\t${pack.version}`));
    console.log(chalk.green("database connection is established"));
    console.log(chalk.yellow('.......................................'));
  })
);

dbConnection = () => {
  const pool = require('./db')
  app.set('pool', pool);
  start();
}

dbConnection();