const { body, check } = require('express-validator');

module.exports = {
  fcn: () => {
    return [
      check("fcn", "Error de formato, acción no declarada.").not().isEmpty(),
    ]
  }
}