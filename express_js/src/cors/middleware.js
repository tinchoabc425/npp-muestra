let jwt = require('jsonwebtoken');
const reqResponse = require('./responseHandler');
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  checkToken
}

function checkToken(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (token) {
    let key = process.env.TOKEN_SECRET
    jwt.verify(token.split(' ')[1], key, {
      /* ignoreExpiration: true */
    }, (err, decoded) => {
      if (err) {
        return res.status(414).send(reqResponse.errorResponse(414));
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(415).send(reqResponse.errorResponse(415));
  }

}
