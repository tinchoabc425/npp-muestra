const router = require('express').Router();
const UserProfileController = require('./UserProfileController');
const RouteConstant = require('../../constant/Routes');
const Middleware = require('../../cors/middleware').checkToken;
/* const Validation = require('../../validation/UserValidation') */

module.exports = (app) => {
  
  router.route('/me')
  .get(
    /* Validation.create(), */
    UserProfileController.getUser
  );

  router.route('/update')
    .post(
      /* Validation.update(), */
      UserProfileController.updateUser
    );
    
  app.use(
    RouteConstant.VERSION1+RouteConstant.USER+'/login',
    UserProfileController.loginUser
  );
  app.use(
    RouteConstant.VERSION1+RouteConstant.USER,
    Middleware,
    router
  );
};