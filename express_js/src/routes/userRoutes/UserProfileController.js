const UserProfileServices = require('../../services/UserProfileServices');
const reqResponse = require('../../cors/responseHandler');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

function generateAccessToken(user) {
	console.log(process.env.TOKEN_SECRET);
  return jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '21600s' }); //6 horas
}

module.exports = {
	getUser: async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(402).send(reqResponse.errorResponse(402));
		}
		try {
			res.status(201).send({user: req.decoded});
		} catch (error) {
			console.error(error);
			res.status(502).send(reqResponse.errorResponse(502))
		}
	},

	loginUser: (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(402).send(reqResponse.errorResponse(402));
		}
		let pool = req.app.get('pool');
		let data = req.body;
		UserProfileServices.loginUser(data, pool)
			.then((result) => {
				const token = generateAccessToken(result[0])
				res.status(200).send({
					"type": "bearer",
					"token": token,
					"refreshToken": null,
					"user": result[0]
				});
			})
			.catch((error) => {
				console.log(error);
				res.status(502).send(reqResponse.errorResponse(502, error));
			})
	},

	updateUser: (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(402).send(reqResponse.errorResponse(402));
		}
		let data = req.body;
		let params = req.params;
		let query = req.query;
		UserProfileServices.updateUser(data, params, query)
			.then((result) => {
				res.status(201).send(reqResponse.successResponse(201, "User Updated", "User has been updated successfully"));
			})
			.catch((error) => {
				console.error(error);
				res.status(502).send(reqResponse.errorResponse(502));
			})
	}
}


