const { Pool } = require('pg')
const config = require('config').get(mode);
const databaseConfig = config.database;

const pool = new Pool({
  user: databaseConfig.user,
  host: databaseConfig.host,
  database: databaseConfig.database,
  password: databaseConfig.password,
  port: databaseConfig.port,
  ssl: { rejectUnauthorized: false }
})
module.exports = {
  query: (text, params, callback) => {
    const start = Date.now()

    return pool.query(text, params, (err, res) => {
      if(typeof res !== 'undefined'){
        const duration = Date.now() - start
        console.log('QUERY: '+text.replace(/\$(\d)/g, function (mathchedText,index,offset,str) {
          return '\''+params[index-1]+'\''
        }));
        console.log('executed query', { text, params, duration, rows: res.rowCount })
      }
      callback(err, res)
    })
  }
}